



const buttons = document.querySelectorAll('.btn');

document.addEventListener('keydown', e => {
  const regExp = new RegExp('^[a-z]')
  let key = e.key;
  if (key.match(regExp)){
    key = key.toUpperCase();
  }
  buttons.forEach((btn) => {
    if(btn.style.backgroundColor === 'blue' || e.key !== btn.textContent){
      btn.style.backgroundColor = 'white';
      btn.style.color = 'black';
    }

    if (key === btn.textContent){
      btn.style.backgroundColor = 'blue';
      btn.style.color = 'white';
    }
  })
})

// Вопрос : как избавиться от белого фона и рамок на всех клавишах при нажатии какой либо клавиши на клавиатуре?